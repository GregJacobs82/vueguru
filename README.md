# Vue Guru

### Fresh Start 20181127

---

#### Start Local Development

1. `$ yarn install`
2. `$ yarn serve`

--- 

#### DEPLOYMENT

###### NOTE: Goes live `vueguru.com` immediately. There is not build / test

1. `$ firebase deploy --only hosting`

---

#### Tutorial / Walkthrough

Reference: https://medium.com/@sumn2u/automate-vuejs-app-deployment-to-firebase-using-gitlab-f2d6e07717c1

---

#### Project Mirrored on GitLab.com

1. Sign in with Github (greg@2book.com) - pw: `chickentime`

--- 

##### ******NOTES:** 

Can't get auto-deployment to work when 'master' branch updates. 

The build fails in GitLab. 

There might be something with `.gitlab-ci.yml` file.

I have set the `FIREBASE_DEPLOYMENT_KEY` in the Settings > CI/CD Section of GitLab... but build still fails. (See Failed build history at CI/CD Section: https://gitlab.com/GregJacobs82/vueguru/pipelines)


**UPDATE:**
1. Getting error at gitlab deployment build for `yarn install`
```
$ yarn install
 yarn install v1.12.3
 [1/4] Resolving packages...
 warning Lockfile has incorrect entry for "@vue/cli-plugin-babel@^3.2.1". Ignoring it.
 error Couldn't find any versions for "@vue/cli-plugin-babel" that matches "^3.2.1"
 info Visit https://yarnpkg.com/en/docs/cli/install for documentation about this command.
 ERROR: Job failed: exit code 1
 ```
 2. Locally, my `yarn run v1.6.0` ... but on GitLab deployment is using `yarn install v1.12.3`, and erroring at the `cli-plugin-babel version 3.2.1`